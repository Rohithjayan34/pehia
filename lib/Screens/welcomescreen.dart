import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:welocome_pehia/Screens/loginscreen.dart';
import 'package:welocome_pehia/Screens/signup.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            SizedBox(height: 50,),
            Center(
                child: Text("Pehia Summit",
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
                ),
            ),
            SizedBox(height: 100,),
            SvgPicture.asset(
              "assets/icons/chat.svg",
              height: 300,
            ),
            SizedBox(height: 50,),

            Container(
              height: MediaQuery.of(context).size.height/14,
              width:MediaQuery.of(context).size.width/1.3,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                  );
                },
                child: Text("Login"),
              ),

            ),
            SizedBox(height: 30,),

            Container(
              height: MediaQuery.of(context).size.height/14,
              width:MediaQuery.of(context).size.width/1.3,
              decoration: BoxDecoration(
                color:Colors.blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Signup()),
                  );

                },
                child: Text("Signup"),
              ),

            ),

          ],
        ),
      ),
    );
  }
}
