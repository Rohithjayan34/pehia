import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:welocome_pehia/Screens/loginscreen.dart';
 class Signup extends StatefulWidget {
   @override
   _SignupState createState() => _SignupState();
 }

 class _SignupState extends State<Signup> {
   @override
   Widget build(BuildContext context) {
     return Scaffold(
       body:  Column(
         children: [
           SizedBox(height: 80,),
           Center(
             child: Padding(
               padding: const EdgeInsets.all(10.0),
               child: Text(
                 "Signup Section",
                 style: TextStyle(
                     fontSize: 30,
                     fontWeight: FontWeight.bold,
                     color: Colors.red),
               ),
             ),
           ),
           SizedBox(
             height: 100,
           ),
           SvgPicture.asset(
             "assets/icons/signup.svg",
             height: 300,
           ),
           SizedBox(
             height: 50,
           ),
           Container(
             height: 60,
             width: 300,
             decoration: BoxDecoration(
               color: Colors.lightBlue,
               borderRadius: BorderRadius.circular(10),
             ),
             child: TextField(
               decoration: InputDecoration(
                 icon: Icon(
                   Icons.person,
                   color: Colors.white,
                 ),
                 hintText: "Enter Your Email",
                 border: InputBorder.none,
               ),
             ),
           ),

           SizedBox(height: 25,),

           Container(
             height: 60,
             width: 300,
             decoration: BoxDecoration(
               color: Colors.lightBlue,
               borderRadius: BorderRadius.circular(10),
             ),
             child: TextField(
               decoration: InputDecoration(
                 icon: Icon(
                   Icons.visibility,
                   color: Colors.white,
                 ),

                 hintText: "Enter Your Password",
                 border: InputBorder.none,
               ),
             ),
           ),
           SizedBox(height: 25,),

           Container(
             height: MediaQuery.of(context).size.height/14,
             width:MediaQuery.of(context).size.width/2,
             decoration: BoxDecoration(
               color:Colors.orange,
               borderRadius: BorderRadius.circular(10),
             ),
             child: FlatButton(
               onPressed: () {  },
               child: Text("Signup"),

             ),
           ),


           Padding(
             padding: const EdgeInsets.all(10.0),
             child: FlatButton(onPressed: () {
               Navigator.push(
                 context,
                 MaterialPageRoute(builder: (context) => LoginScreen()),
               );

             },
                 child: Text("If  Already Signed? Log in, ")),
           )
         ],
       ),
     );
   }
 }
