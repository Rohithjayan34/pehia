import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:welocome_pehia/Screens/signup.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(left:50.0),
                        child: Text(
                          "Login Section",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Colors.red),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 100,
            ),
            SvgPicture.asset(
              "assets/icons/login.svg",
              height: 300,
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              height: 60,
              width: 300,
              decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: TextField(
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  hintText: "Enter Your Email",
                  border: InputBorder.none,
                ),
              ),
            ),

            SizedBox(height: 25,),

            Container(
              height: 60,
              width: 300,
              decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: TextField(
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.visibility,
                    color: Colors.white,
                  ),

                  hintText: "Enter Your Password",
                  border: InputBorder.none,
                ),
              ),
            ),

            SizedBox(height: 25,),

            Container(
              height: MediaQuery.of(context).size.height/14,
              width:MediaQuery.of(context).size.width/2,
              decoration: BoxDecoration(
                color:Colors.orange,
                borderRadius: BorderRadius.circular(10),
              ),
              child: FlatButton(
                onPressed: () {  },
                child: Text("Login"),

              ),
            ),

            Padding(
              padding: const EdgeInsets.all(10.0),
              child: FlatButton(onPressed: () {

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Signup()),
                );
              },
              child: Text("If Not Logged in, Signup")),
            )
          ],
        ),
      ),
    );
  }
}
